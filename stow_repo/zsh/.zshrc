#import aliases
source $HOME/.config/aliases
autoload -U add-zsh-hook

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$HOME/.local/bin:$PATH"
fi
# set PATH so it includes snaps's bin if it exists
if [ -d "/snap/bin" ] ; then
    export PATH="/snap/bin:$PATH"
fi

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.config/zsh"

# STOP KALI CONFIG VARIABLES# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME=""

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

setopt autocd              # change directory just by typing its name
setopt correct             # auto correct mistakes
setopt interactivecomments # allow comments in interactive mode
setopt magicequalsubst     # enable filename expansion for arguments of the form ‘anything=expression’
setopt nonomatch           # hide error message if there is no match for the pattern
setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense
setopt promptsubst         # enable command substitution in prompt
setopt prompt_subst

WORDCHARS=${WORDCHARS//\/} # Don't consider certain characters part of the word

# hide EOL sign ('%')
PROMPT_EOL_MARK=""

# configure key keybindings
bindkey -v                                        # vi key bindings
bindkey ' ' magic-space                           # do history expansion on space
bindkey '^U' backward-kill-line                   # ctrl + U
bindkey '^[[3;5~' kill-word                       # ctrl + Supr
bindkey '^[[3~' delete-char                       # delete
bindkey '^[[1;5C' forward-word                    # ctrl + ->
bindkey '^[[1;5D' backward-word                   # ctrl + <-
bindkey '^[[5~' beginning-of-buffer-or-history    # page up
bindkey '^[[6~' end-of-buffer-or-history          # page down
bindkey '^[[H' beginning-of-line                  # home
bindkey '^[[F' end-of-line                        # end
bindkey '^[[Z' undo                               # shift + tab undo last action

# enable completion features
autoload -Uz compinit
compinit -d ~/.cache/zcompdump
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' rehash true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# History configurations
HISTFILE=~/.zsh/.zsh_history
HISTSIZE=1000
SAVEHIST=2000
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history          # share command history data

# force zsh to show the complete history
alias history="history 0"

# configure `time` format
TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P'

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

# The following block is surrounded by two delimiters.
# These delimiters must not be modified. Thanks.
# START KALI CONFIG VARIABLES
PROMPT_ALTERNATIVE=twoline
NEWLINE_BEFORE_PROMPT=yes
# STOP KALI CONFIG VARIABLES

### NVM
ZSH_THEME_NVM_PROMPT_PREFIX="%B⬡%b "
ZSH_THEME_NVM_PROMPT_SUFFIX=""

# Ruby prompt settings
ZSH_THEME_RUBY_PROMPT_PREFIX="%{$fg[grey]%}"
ZSH_THEME_RUBY_PROMPT_SUFFIX="%{$reset_color%}"

#Theme Color
ZSH_THEME_BORDER_COLOR="%(#.blue.green)"
# Colors vary depending on time lapsed.
ZSH_THEME_TIMESPAN_SHORT="%F{green}"
ZSH_THEME_TIMESPAN_MEDIUM="%F{yellow}"
ZSH_THEME_TIMESPAN_LONG="%F{red}"
ZSH_THEME_TIMESPAN_NEUTRAL="%F{white}"

#Icons & Symbols
ADMIN_AT_ICON="💀"
ALARM_CLOCK_ICON="⏰️"
GIT_BRANCH_ICON="\ue0a0" # 
HOUR_GLASS_ICON="⏳️"
USER_AT_ICON="㉿"
FALLBACK_AT_SYMBOL="@"

ZSH_THEME_SQUARE_PREFIX="%F{$ZSH_THEME_BORDER_COLOR}[%f"
ZSH_THEME_SQUARE_SUFFIX="%F{$ZSH_THEME_BORDER_COLOR}]%f"
ZSH_THEME_BORDER_AFFIX="%F{$ZSH_THEME_BORDER_COLOR}─%f"
ZSH_THEME_ROUND_PREFIX="%F{$ZSH_THEME_BORDER_COLOR}(%f"
ZSH_THEME_ROUND_SUFFIX="%F{$ZSH_THEME_BORDER_COLOR})%f"

### Git promt settings [±master ▾●]
ZSH_THEME_GIT_STATUS_ADDED="%F{green}✚%F{reset}"
ZSH_THEME_GIT_STATUS_AHEAD="%F{cyan}▴%F{reset}"
ZSH_THEME_GIT_STATUS_BEHIND="%F{magenta}▾%F{reset}"
ZSH_THEME_GIT_STATUS_CLEAN="%F{green}✓%F{reset}"
ZSH_THEME_GIT_STATUS_DELETED="%F{red}%B✖%b%F{reset%}"
ZSH_THEME_GIT_STATUS_DIRTY="%F{red}%B✗%b%F{reset%}"
ZSH_THEME_GIT_STATUS_MODIFIED="%F{yellow}%B⚑%b%F{reset}"
ZSH_THEME_GIT_STATUS_RENAMED="%F{blue}%B»%b%F{reset}"
ZSH_THEME_GIT_STATUS_STAGED="%F{green}%B●%b%F{reset}"
ZSH_THEME_GIT_STATUS_UNMERGED="%F{cyan}%B◒%b%F{reset}"
ZSH_THEME_GIT_STATUS_UNSTAGED="%F{yellow}%B●%b%F{reset}"
ZSH_THEME_GIT_STATUS_UNTRACKED="%F{red}%B●%b%F{reset}"

# LS colors, made with https://geoff.greer.fm/lscolors/
export LSCOLORS="exfxcxdxbxegedabagacad"
export LS_COLORS='di=34;40:ln=35;40:so=32;40:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:'
export GREP_COLOR='1;33'

function set_fallback_icons {

  if [[ $TERM == linux ]]; then
    ADMIN_AT_ICON="$FALLBACK_AT_SYMBOL"
    ALARM_CLOCK_ICON=""
    HOUR_GLASS_ICON=""
    USER_AT_ICON="$FALLBACK_AT_SYMBOL"
  fi
}

# Determine the time since last commit. If branch is clean,
# use a neutral color, otherwise colors will vary according to time.
function get_commit_age() {
  local last_commit now seconds_since_last_commit
  local minutes hours days sub_hours sub_minutes
  local commit_age color

  if git rev-parse --git-dir > /dev/null 2>&1; then
    # Only proceed if there is actually a commit.
    if [[ $(git log 2>&1 > /dev/null | grep -c "^fatal: bad default revision") == 0 ]]; then
      # Get the last commit.
      last_commit=`git log --pretty=format:'%at' -1 2> /dev/null`
      now=$(date +%s)
      seconds_since_last_commit=$((now-last_commit))

      # Totals
      minutes=$((seconds_since_last_commit / 60))
      hours=$((seconds_since_last_commit/3600))

      # Sub-hours and sub-minutes
      days=$((seconds_since_last_commit / 86400))
      sub_hours=$((hours % 24))
      sub_minutes=$((minutes % 60))

      if [[ -n $(git status -s 2> /dev/null) ]]; then
        if [ "$minutes" -gt 30 ]; then
          color="$ZSH_THEME_TIMESPAN_LONG"
        elif [ "$minutes" -gt 10 ]; then
          color="$ZSH_THEME_TIMESPAN_MEDIUM"
        else
          color="$ZSH_THEME_TIMESPAN_SHORT"
        fi
      else
        color="$ZSH_THEME_TIMESPAN_NEUTRAL"
      fi

      if [ "$hours" -gt 24 ]; then
        commit_age="$color${days}d${sub_hours}h${sub_minutes}%f"
      elif [ "$minutes" -gt 60 ]; then
        commit_age="$color${hours}h${sub_minutes}%f"
      else
        commit_age="$color${minutes}%f"
      fi
      
      echo "$ZSH_THEME_ROUND_PREFIX$HOUR_GLASS_ICON$commit_age$ZSH_THEME_ROUND_SUFFIX$ZSH_THEME_BORDER_AFFIX"

    fi
  else
    echo ""
  fi
}

# Formats prompt string for current git commit short SHA
function get_short_sha() {
  local SHA
  if git rev-parse --git-dir > /dev/null 2>&1; then
    SHA=$(command git rev-parse --short HEAD 2> /dev/null) 
    echo "$ZSH_THEME_SQUARE_PREFIX$SHA$ZSH_THEME_SQUARE_SUFFIX"
  fi
}

function get_branch() {
  ref=$(command git symbolic-ref HEAD 2> /dev/null) || \
  ref=$(command git rev-parse --short HEAD 2> /dev/null) || return
  echo "${ref#refs/heads/}"
}

function get_status() {
  _STATUS=""

  # check status of files
  _INDEX=$(command git status --porcelain 2> /dev/null)
  if [[ -n "$_INDEX" ]]; then
    if $(echo "$_INDEX" | command grep -q '^[AMRD]. '); then
      _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_STAGED"
    fi
    if $(echo "$_INDEX" | command grep -q '^.[MTD] '); then
      _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_UNSTAGED"
    fi
    if $(echo "$_INDEX" | command grep -q -E '^\?\? '); then
      _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_UNTRACKED"
    fi
    if $(echo "$_INDEX" | command grep -q '^UU '); then
      _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_UNMERGED"
    fi
  else
    _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_CLEAN"
  fi

  # check status of local repository
  _INDEX=$(command git status --porcelain -b 2> /dev/null)
  if $(echo "$_INDEX" | command grep -q '^## .*ahead'); then
    _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_AHEAD"
  fi
  if $(echo "$_INDEX" | command grep -q '^## .*behind'); then
    _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_BEHIND"
  fi
  if $(echo "$_INDEX" | command grep -q '^## .*diverged'); then
    _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_DIVERGED"
  fi

  if $(command git rev-parse --verify refs/stash &> /dev/null); then
    _STATUS="$_STATUS$ZSH_THEME_GIT_STATUS_STASHED"
  fi

  echo $_STATUS
}

function get_git_info() {
  local _branch=$(get_branch)
  local _status=$(get_status)
  local _result="$ZSH_THEME_SQUARE_PREFIX"
  local _suffix="$ZSH_THEME_SQUARE_SUFFIX"
  local _result=""
  
  if [[ "${_branch}x" != "x" ]]; then
    _result="$ZSH_THEME_SQUARE_PREFIX$GIT_BRANCH_ICON $_branch"
    if [[ "${_status}x" != "x" ]]; then
      _result="$_result $_status"
    fi
    _result="$_result$_suffix"
  fi
  echo $_result
}

function get_Spaces() {
  local STR=$1$2
  local zero='%([BSUbfksu]|([FB]|){*})'
  local LENGTH=${#${(S%%)STR//$~zero/}}
  local SPACES=""
  (( LENGTH = ${COLUMNS} - $LENGTH - 2))

  for i in {0..$LENGTH}
    do
      SPACES="$SPACES "
    done

  echo $SPACES
}

function get_Line() {
  local STR=$1$2
  local zero='%([BSUbfksu]|([FB]|){*})'
  local LENGTH=${#${(S%%)STR//$~zero/}}
  local LINE=""
  (( LENGTH = ${COLUMNS} - $LENGTH - 3))

  for i in {0..$LENGTH}
    do
      LINE="$LINE%F{$ZSH_THEME_BORDER_COLOR}─%f"
    done

  echo $LINE
}

_precmd() {

  local _lprefix="%F{%(#.blue.green)}┌──(%f"
  local _chroot="%F{%(#.blue.green)}${debian_chroot:+($debian_chroot)─}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))─}%f"
  local _user="%B%F{%(#.red.blue)}%n%f%b"
  local _at="%B%(!.%F{red}$ADMIN_AT_ICON%f.%F{blue}$USER_AT_ICON%f)%b"
  local _host="%B%F{%(#.red.blue)}%m%f%b"
  local _laffix="%F{%(#.blue.green)})─[%f"
  local _raffix="%F{%(#.blue.green)}─%f"
  local _path="%B%F{reset}%(6~.%-1~/…/%4~.%5~)%f%b"
  local _lsuffix="%F{%(#.blue.green)}]%f"
  local _rsuffix="%F{%(#.blue.green)}──┐%f"

  local _1LEFT="$_lprefix$_chroot$_user$_at$_host$_laffix$_path$_lsuffix"
  #local _1RIGHT="$(get_short_sha)$_raffix$(get_commit_age)$_raffix$(get_git_info)$_rsuffix"
  local _1RIGHT="$(get_short_sha)$_raffix$(get_git_info)$_rsuffix"

  #_1SPACES=`get_Spaces $_1LEFT $_1RIGHT`
  _1LINE=`get_Line $_1LEFT $_1RIGHT`
  print
  #print -rP "$_1LEFT$_1SPACES$_1RIGHT"
  print -rP "$_1LEFT$_1LINE$_1RIGHT"
}

add-zsh-hook precmd _precmd

configure_prompt() {
    # zsh placeholder: (https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html#Prompt-Expansion)
    # %m -> first part of the hostname
    # %n -> $USERNAME
    # %# -> shows # if user is root, else %.
    # zsh formatting:
    # %B/%b -> start/stop bold mode
    # %F/%f -> start/stop foreground mode
    local _prefix="%F{%(#.blue.green)}└─[%f"
    local _affix="%F{%(#.blue.green)}]-[%f"
    local _suffix="%F{%(#.blue.green)}]%f"
    local _history="✎%h"
    local _exit_code="%B%(?.%F{green}%B√%b%F{reset}.%(?..%b%?%F{red}%B⨯%b%F{reset})%(1j.%j%F{yellow}%B⚙%b%F{reset}.))%f%b"
    local _input="%B%(!.%F{red}#%f.%F{blue}$ %f)%b"

    local _tprefix="%F{%(#.blue.green)}[%f"
    local _time="$ALARM_CLOCK_ICON%*"
    local _tsuffix="%F{%(#.blue.green)}]─┘%f"

    case "$PROMPT_ALTERNATIVE" in
        twoline)
            #PROMPT=$'$p_l1_prefix$p_chroot$p_user$p_at_symbol$p_host$p_l1_affix$p_path$p_l1_suffix\n$p_l2_prefix$p_exit_code$p_l2_suffix$p_l2_input$p_reset'
            #PROMPT=$'$p_l1_prefix$p_chroot$p_user$p_at_symbol$p_host$p_affix$p_path$p_l1_suffix\n$p_l2_prefix$p_exit_code$p_l2_suffix$p_l2_input${reset} '
            PROMPT="$_prefix$_exit_code$_affix$_history$_suffix$_input${reset}"
            #PROMPT=$'%F{%(#.blue.green)}┌──${debian_chroot:+($debian_chroot)─}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))─}(%B%F{%(#.red.blue)}%n$prompt_symbol%m%b%F{%(#.blue.green)})-[%B%F{reset}%(6~.%-1~/…/%4~.%5~)%%F{%(#.blue.green)}]$%{$(echotc UP 1)%}$(_git_time_since_commit)\n└─%B%(#.%F{red}#.%F{blue}$)%b%F{reset} '
            #RPROMPT=$'%(?.. %? %F{red}%B⨯%b%F{reset})%(1j. %j %F{yellow}%B⚙%b%F{reset}.)'
	    #RPROMPT='[$(git_prompt_short_sha)]   $(get_git_info)'
	    RPROMPT=$'$(get_commit_age)'"$_tprefix$_time$_tsuffix"
            ;;
        oneline)
            PROMPT=$'${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%B%F{%(#.red.blue)}%n@%m%b%F{reset}:%B%F{%(#.blue.green)}%~%b%F{reset}%(#.#.$) '
            RPROMPT=
            ;;
        backtrack)
            PROMPT=$'${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%B%F{red}%n@%m%b%F{reset}:%B%F{blue}%~%b%F{reset}%(#.#.$) '
            RPROMPT=
            ;;
    esac
}

if [ "$color_prompt" = yes ]; then
    # override default virtualenv indicator in prompt
    VIRTUAL_ENV_DISABLE_PROMPT=1

    set_fallback_icons
    configure_prompt

    # enable syntax-highlighting
    if [ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && [ "$color_prompt" = yes ]; then
        . /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
        ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
        ZSH_HIGHLIGHT_STYLES[default]=none
        ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=red,bold
        ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=cyan,bold
        ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[global-alias]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[precommand]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[autodirectory]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[path]=underline
        ZSH_HIGHLIGHT_STYLES[path_pathseparator]=
        ZSH_HIGHLIGHT_STYLES[path_prefix_pathseparator]=
        ZSH_HIGHLIGHT_STYLES[globbing]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[command-substitution]=none
        ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[process-substitution]=none
        ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=none
        ZSH_HIGHLIGHT_STYLES[back-quoted-argument-delimiter]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[rc-quote]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[assign]=none
        ZSH_HIGHLIGHT_STYLES[redirection]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[comment]=fg=black,bold
        ZSH_HIGHLIGHT_STYLES[named-fd]=none
        ZSH_HIGHLIGHT_STYLES[numeric-fd]=none
        ZSH_HIGHLIGHT_STYLES[arg0]=fg=green
        ZSH_HIGHLIGHT_STYLES[bracket-error]=fg=red,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-1]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-2]=fg=green,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-3]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-4]=fg=yellow,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-5]=fg=cyan,bold
        ZSH_HIGHLIGHT_STYLES[cursor-matchingbracket]=standout
    fi
else
    PROMPT='${debian_chroot:+($debian_chroot)}%n@%m:%~%# '
fi
unset color_prompt force_color_prompt

toggle_oneline_prompt(){
    if [ "$PROMPT_ALTERNATIVE" = oneline ]; then
        PROMPT_ALTERNATIVE=twoline
    else
        PROMPT_ALTERNATIVE=oneline
    fi
    set_fallback_icons
    configure_prompt
    zle reset-prompt
}
zle -N toggle_oneline_prompt
bindkey ^P toggle_oneline_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|Eterm|aterm|kterm|gnome*|alacritty)
    TERM_TITLE=$'\e]0;${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%n@%m: %~\a'
    ;;
*)
    ;;
esac

precmd() {
    # Print the previously configured title
    print -Pnr -- "$TERM_TITLE"

    # Print a new line before the prompt, but only if it is not the first line
    if [ "$NEWLINE_BEFORE_PROMPT" = yes ]; then
        if [ -z "$_NEW_LINE_BEFORE_PROMPT" ]; then
            _NEW_LINE_BEFORE_PROMPT=1
        else
            print ""
        fi
    fi
}

# enable color support of ls, less and man, and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'

    export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
    export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
    export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
    export LESS_TERMCAP_so=$'\E[01;33m'    # begin reverse video
    export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
    export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
    export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

    # Take advantage of $LS_COLORS for completion as well
    zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
    zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
fi

# enable auto-suggestions based on the history
if [ -f /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ]; then
    . /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
    # change suggestion color
    ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#999'
fi

# enable command-not-found if installed
if [ -f /etc/zsh_command_not_found ]; then
    . /etc/zsh_command_not_found
fi

# Set name of the theme to load --- if set to "random", it will
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
          git
          sudo
          z
        )

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

